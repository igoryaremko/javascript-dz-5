// Завдання
// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без 
// використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем,
//  все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у 
// консоль результат виконання функції.

// Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити
//  функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.

function createNewUser()
{
  let newUser = new Object();
  newUser.firstName = prompt("Enter you first name");
  newUser.lastName = prompt("Enter you last name");
  newUser.getLogin = function () {
  return `${(newUser.firstName.charAt(0) + newUser.lastName).toLowerCase()}`;
};
  return newUser;
}

let user = createNewUser();
console.log(user.getLogin());

